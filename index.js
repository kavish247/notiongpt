import { Client } from "@notionhq/client"
import 'dotenv/config'
import { Configuration, OpenAIApi } from "openai";

const configuration = new Configuration({
  apiKey: process.env.OPENAI_API_KEY,
});
const openai = new OpenAIApi(configuration);
  
const notionkey = process.env.NOTION_KEY;
const notion = new Client({auth:notionkey}); 
const database_id = process.env.NOTION_DATABASE_ID;

let prompt = await getLatestPage(); 
let result = await createGPTQuery(prompt);
console.log(result)
/* (async () => {
  
   
    //main(); 
})() */


async function getLatestPage () {
    const response =  await notion.databases.query({
        database_id: database_id,
        sorts: [
                {
                    timestamp:"last_edited_time",
                    direction: "descending" 
            }
        ]
    })
    console.log(response)
    console.log(response.results[0].properties.Name.title[0].text.content)
    return response.results[0].properties.Name.title[0].text.content;
}
async function createGPTQuery(prompt){
    let result = {};
    try {
        const completion = await openai.createCompletion({
        model: "text-davinci-003",
        prompt: prompt,
        max_tokens: 2000,
        temperature: 0,
        });
        console.log(completion.data.choices[0].text )    

        result = completion.data.choices[0];     
      
    } catch(error) {
        // Consider adjusting the error handling logic for your use case
        if (error.response) {
            console.error(error.response.status, error.response.data);
        } else {
            console.error(`Error with OpenAI API request: ${error.message}`);
        }
        return result;
    }
    return result

}